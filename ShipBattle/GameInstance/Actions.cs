﻿using System;
using System.Drawing;
using ShipBattle;

namespace ShipBattle
{
    //Marker Interface for everything that needs to be deserialized from json
    public interface IActions
    {
    }

    public interface IGameActions : IActions
    {
        Guid GameId { get; set; }
    }

    public interface IPlayerActions : IActions
    {
        Guid PlayerId { get; set; }
    }

    public class StartGame : IGameActions
    {
        public Guid GameId { get; set; }
    }

    public class ClientConnection : IPlayerActions
    {
        public Guid PlayerId { get; set; }
    }

    public class ClientDisconnect : IPlayerActions
    {
        public Guid PlayerId { get; set; }
    }

    public class JoinGame : IGameActions, IPlayerActions
    {
        public Guid GameId { get; set; }
        public Guid PlayerId { get; set; }
        public bool IsBot { get; set; }
    }

    public class JoinSinglePlayerGame : IGameActions, IPlayerActions
    {
        public Guid GameId { get; set; }
        public Guid PlayerId { get; set; }
    }


    public class PlaceShip : IGameActions, IPlayerActions
    {
        public Guid GameId { get; set; }
        public Guid PlayerId { get; set; }

        public ShipType Ship { get; set; }
        public Point StartPoint { get; set; }
        public bool IsUpward { get; set; }
    }

    public class Ready : IGameActions, IPlayerActions
    {
        public Guid GameId { get; set; }
        public Guid PlayerId { get; set; }
    }

    public class FireShot : IGameActions, IPlayerActions
    {
        public Guid GameId { get; set; }
        public Guid PlayerId { get; set; }

        public Point Position { get; set; }
    }


    public enum ShipType
    {
        Carrier = 1,
        Battleship = 2,
        Cruiser = 3,
        Submarine = 4,
        Destroyer = 5,
    }
}