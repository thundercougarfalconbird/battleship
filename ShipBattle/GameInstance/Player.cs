﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.AspNet.SignalR;

namespace ShipBattle
{
    public class Player : ClientCommunication
    {
        public Guid PlayerAddress { get; private set; }

        public string PlayerName { get; set; }
        public bool IsBot { get; private set; }

        public Player(Guid playerAddress, IHubContext hub) :
            base(hub)
        {
            PlayerAddress = playerAddress;
        }

        public void EnableBot()
        {
            IsBot = true;
        }
    }

    public class Game_Player : Player
    {
        public bool IsReady { get; set; }
        public List<Point> ShotList = new List<Point>();
        public List<Ship> ships = new List<Ship>();

        public Game_Player(Guid playerAddress, IHubContext hub) : base(playerAddress, hub) { }
    }
}