using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace ShipBattle
{
    public class ShipBattleGameInstance : MessageBaseGame
    {
        private readonly ConcurrentDictionary<Guid, Game_Player> myPlayers = new ConcurrentDictionary<Guid, Game_Player>();
        private readonly List<IActions> allGameActions = new List<IActions>();
        private Game_Player _currentPlayer;

        public ShipBattleGameInstance(IHubContext hub) :
            base(hub) { }

        private Game_Player CurrentPlayer
        {
            get { return _currentPlayer; }
            set
            {
                _currentPlayer = value;
                hub.Clients.Group(GameId.ToString()).game_NewPlayerTurn(CurrentPlayer.PlayerName);
                DoBotAction();
            }
        }

        internal override async void ProcessAction(IActions playerAction)
        {
            allGameActions.Add(playerAction);
            await ((dynamic)this).HandleAction((dynamic)playerAction);
        }

        #region Action Handlers

        private async Task HandleAction(PlaceShip a)
        {
            myPlayers[a.PlayerId].IsReady = false;
            hub.Clients.Group(GameId.ToString()).game_PlayerReady(a.PlayerId, false);

            var isValid = false;
            //Add New Ship to list
            var s = new Ship
            {
                Type = a.Ship,
                IsUpDirection = a.IsUpward,
                Location = a.StartPoint
            };
            var playerShips = myPlayers[a.PlayerId].ships;
            try
            {
                //See If Ship already exists
                if (playerShips.Any(c => c.Type == a.Ship))
                    playerShips.RemoveAll(c => c.Type == a.Ship);

                //upper left cell is 0,0
                if (!myPlayers.ContainsKey(a.PlayerId) || IsStarted) return;


                if (!Validate_ShipPlacement(s, myPlayers[a.PlayerId])) return;

                isValid = true;
                playerShips.Add(s);
            }
            finally
            {
                if (!isValid)
                    s.Location = new Point(-1, -1);

                hub.Clients.Client(a.PlayerId.ToString())
                    .game_PlayerPiecePlaced((int)s.Type, s.Location, s.IsUpDirection);
                hub.Clients.Client(a.PlayerId.ToString())
                    .game_PlayerReadyToReadyState(playerShips.Count(c => c.Location != new Point(-1, -1)) == 5);
            }
        }

        private async Task HandleAction(Ready a)
        {
            myPlayers[a.PlayerId].IsReady = true;
            hub.Clients.Group(GameId.ToString()).game_PlayerReady(a.PlayerId, true);
            Check_GameReadyToStart();
        }

        private async Task HandleAction(FireShot a)
        {
            //Ensure correct player is firing
            if (a.PlayerId != CurrentPlayer.PlayerAddress) return;
            try
            {
                var offensivePlayer = myPlayers[a.PlayerId];
                var defensivePlayer = myPlayers.FirstOrDefault(k => k.Key != a.PlayerId).Value;
                //Get the other players ships
                //See if this point hits any of the other players ships
                var shotSucess =
                    defensivePlayer.ships.Any(testShip => testShip.points.Any(c => c.location == a.Position));
                hub.Clients.Group(GameId.ToString())
                    .game_PlayerShotResult(offensivePlayer.PlayerName, defensivePlayer.PlayerName, shotSucess,
                        a.Position);

                //if so, mark that spot as hit
                if (!shotSucess) return;

                foreach (var ship in defensivePlayer.ships)
                    foreach (var p in ship.points.Where(aa => aa.location == a.Position))
                    {
                        p.IsHit = true;
                        Check_ShipSunk(ship, offensivePlayer, defensivePlayer);
                    }
            }
            finally
            {
                CurrentPlayer = myPlayers.FirstOrDefault(c => c.Key != CurrentPlayer.PlayerAddress).Value;
            }
        }

        private async Task HandleAction(JoinGame a)
        {
            if (!myPlayers.ContainsKey(a.PlayerId))
            {
                var player = new Game_Player(a.PlayerId, hub)
                {
                    PlayerName = "Player " + (myPlayers.Count + 1)
                };

                hub.Clients.Client(a.PlayerId.ToString()).YourPlayerId(player.PlayerName);

                myPlayers.GetOrAdd(a.PlayerId, player);
                if (!a.IsBot)
                    await hub.Groups.Add(a.PlayerId.ToString(), GameId.ToString());
                if (myPlayers.Count >= 2)
                    AwaitingPlayers = false;
                hub.Clients.Group(GameId.ToString()).game_PlayerJoined(GameId, a.PlayerId);
                hub.Clients.Client(a.PlayerId.ToString()).game_YouJoinedAGame(GameId);
            }
        }

        private async Task HandleAction(ClientDisconnect a)
        {
            if (myPlayers.ContainsKey(a.PlayerId))
            {
                myPlayers[a.PlayerId].EnableBot();
                DoBotAction();
            }
        }

        #endregion

        private void DoBotAction()
        {
            var r = new Random();
            if (!IsStarted && !GameOver)
            {
                var bots = myPlayers
                    .Where(e => e.Value.IsBot && !e.Value.IsReady)
                    .Select(e => e.Value);

                foreach (var bot in bots)
                {
                    bot.ships.Clear();
                    //place ships
                    foreach (ShipType shipType in Enum.GetValues(typeof(ShipType)))
                    {
                        Ship ship;
                        do
                        {
                            ship = new Ship()
                            {
                                Type = shipType,
                                IsUpDirection = r.Next(0, 2) == 1,
                                Location = new Point(r.Next(0, 10), r.Next(0, 10))
                            };
                        } while (!Validate_ShipPlacement(ship, bot));
                        bot.ships.Add(ship);
                    }
                    ActionQueue.Enqueue(new Ready() { GameId = GameId, PlayerId = bot.PlayerAddress });
                }
                return;
            }

            if (CurrentPlayer.IsBot)
            {
                if (IsStarted)
                {
                    //Take a shot
                    Point position;
                    do
                    {
                        position = new Point(r.Next(0, 10), r.Next(0, 10));
                    } while (CurrentPlayer.ShotList.Contains(position));

                    CurrentPlayer.ShotList.Add(position);
                    ActionQueue.Enqueue(new FireShot
                    {
                        GameId = GameId,
                        PlayerId = CurrentPlayer.PlayerAddress,
                        Position = position
                    });
                }
            }
        }

        private async void Check_ShipSunk(Ship ship, Player offensivePlayer, Player defensivePlayer)
        {
            if (ship.points.All(c => c.IsHit))
            {
                await
                    hub.Clients.Group(GameId.ToString())
                        .game_ShipSunk(offensivePlayer.PlayerName, defensivePlayer.PlayerName, ship.Type.ToString());
                Check_GameOver();
            }
        }

        private void Check_GameOver()
        {
            var defeatedPlayer = myPlayers.Values.FirstOrDefault(p => p.ships.All(s => s.points.All(pp => pp.IsHit)));
            if (defeatedPlayer != null)
            {
                var winningPlayer = myPlayers.FirstOrDefault(k => k.Key != defeatedPlayer.PlayerAddress).Value;
                hub.Clients.Group(GameId.ToString()).game_GameOver(winningPlayer.PlayerName, defeatedPlayer.PlayerName);
                GameOver = true;
            }
        }

        private void Check_GameReadyToStart()
        {
            if (myPlayers.Count != 2 || !myPlayers.All(c => c.Value.IsReady)) return;

            IsStarted = true;
            hub.Clients.Group(GameId.ToString()).game_GameStarted(true);

            CurrentPlayer = myPlayers.Values.ToList()[0];
        }

        private bool Validate_ShipPlacement(Ship ship, Game_Player p)
        {
            //verify ship is valid for placement
            //can not overlap edges
            if (ship.points.Any(c => c.location.X < 0 || c.location.Y < 0 || c.location.X > 9 || c.location.Y > 9))
                return false;

            //can not colide with other ships
            return p.ships.All(
                playerShip => !ship.points.Any(
                    testpoint => playerShip.points.Contains(testpoint)
                    )
                );
        }
    }
}