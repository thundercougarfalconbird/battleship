using System.Collections.Generic;
using System.Drawing;

namespace ShipBattle
{
    public class Ship
    {
        private readonly List<ShipPoint> _points = new List<ShipPoint>();
        private Point _location;

        public Point Location
        {
            get { return _location; }
            set
            {
                _location = value;
                _points.Clear();
            }
        }

        public ShipType Type { get; set; }
        public bool IsUpDirection { get; set; }

        public List<ShipPoint> points
        {
            get
            {
                if (_points.Count != 0) return _points;

                _points.Add(new ShipPoint(_location, false));
                int cells = 0;
                switch (Type)
                {
                    case ShipType.Carrier:
                        cells = 5;
                        break;
                    case ShipType.Battleship:
                        cells = 4;
                        break;
                    case ShipType.Cruiser:
                        cells = 3;
                        break;
                    case ShipType.Submarine:
                        cells = 3;
                        break;
                    case ShipType.Destroyer:
                        cells = 2;
                        break;
                }
                if (IsUpDirection)
                {
                    for (int y = 1; y < cells; y++)
                    {
                        _points.Add(new ShipPoint(new Point(_location.X, _location.Y - y), false));
                    }
                }
                else
                {
                    for (int x = 1; x < cells; x++)
                    {
                        _points.Add(new ShipPoint(new Point(_location.X + x, _location.Y), false));
                    }
                }
                return _points;
            }
        }
    }

    public class ShipPoint
    {
        public Point location { get; set; }
        public bool IsHit { get; set; }

        public ShipPoint(Point location, bool IsHit)
        {
            this.location = location;
            this.IsHit = IsHit;
        }

        public override bool Equals(object obj)
        {
            return this.location == ((ShipPoint)obj).location;
        }

        public override int GetHashCode()
        {
            return this.location.GetHashCode();
        }
    }
}