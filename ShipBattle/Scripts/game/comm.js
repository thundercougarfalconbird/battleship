﻿var gameId;
var playerId;


var totalGamesWaitingForPlayers = 0;
var totalGames = 0;

var hub;
var canExecute = false;

$(function() {
    hub = $.connection.mainHub;

    //Start the hub and wire up server call functions after it is started
    $.connection.hub.logging = true; //debugging

    hub.client.updatedGameStats = function(_totalGamesWaitingForPlayers, _totalGames) {
        totalGames = _totalGames;
        totalGamesWaitingForPlayers = _totalGamesWaitingForPlayers;
        if (game.txtPlayersWaiting != null) {
            game.txtPlayersWaiting.text = "Total Games: " + totalGames;
            game.txtPlayersWaiting.updateText();
        }
        if (game.txtPlayersHosting != null) {
            game.txtPlayersHosting.text = "Games Waiting for Players: " + totalGamesWaitingForPlayers;
            game.txtPlayersHosting.updateText();
        }
        if (game.stage != null) game.stage.update();
    }

    hub.client.game_PlayerReadyToReadyState = function(IsReadyToReady) {
        game.btnReady.isVisible = IsReadyToReady;
    }

    hub.client.game_PlayerReady = function(playerId, ready) {
        //TODO:  Lets everyone know which players are ready.
    }

    hub.client.yourPlayerId = function(playerid) {
        //toastr.error(playerid);
        playerId = playerid;
    }

    hub.client.game_GameStarted = function(started) {

        game.carrierX = game.getShipX(0);
        game.carrierY = game.getShipY(0);
        game.carrierRotation = game.getShipRotation(0);

        game.battleshipX = game.getShipX(1);
        game.battleshipY = game.getShipY(1);
        game.battleshipRotation = game.getShipRotation(1);

        game.cruiserX = game.getShipX(2);
        game.cruiserY = game.getShipY(2);
        game.cruiserRotation = game.getShipRotation(2);

        game.subX = game.getShipX(3);
        game.subY = game.getShipY(3);
        game.subRotation = game.getShipRotation(3);

        game.destroyerX = game.getShipX(4);
        game.destroyerY = game.getShipY(4);
        game.destroyerRotation = game.getShipRotation(4);

        game.screenManager.changeScreen("game");
    }

    hub.client.game_YouJoinedAGame = function(gameid) {
        gameId = gameid;
        game.screenManager.changeScreen("start");
    }
    hub.client.game_PlayerJoined = function(gameId, joiningPlayerId) {
        //toastr.info("A player has joined your game " + gameId + " with the Id of " + joiningPlayerId);
        settings.numberOfPlayers++;
    }
    hub.client.game_PlayerPiecePlaced = function(type, location, IsUpDirection) {
        //TODO:  Slyprid
        //toastr.info("ShipType=" + type + " location=" + location + " DirectionIsUp=" + IsUpDirection);

        // Invalid piece placement
        var shipId = type - 1;
        if (location == "-1, -1") {
            switch (shipId) {
            case 0:
                game.carrierSprite.spriteSheet = game.carrierRedSpriteSheet;
                break;
            case 1:
                game.battleshipSprite.spriteSheet = game.battleshipRedSpriteSheet;
                break;
            case 2:
                game.cruiserSprite.spriteSheet = game.cruiserRedSpriteSheet;
                break;
            case 3:
                game.subSprite.spriteSheet = game.submarineRedSpriteSheet;
                break;
            case 4:
                game.destroyerSprite.spriteSheet = game.destroyerRedSpriteSheet;
                break;
            }
        } else {
            switch (shipId) {
            case 0:
                game.carrierSprite.spriteSheet = game.carrierSpriteSheet;
                break;
            case 1:
                game.battleshipSprite.spriteSheet = game.battleshipSpriteSheet;
                break;
            case 2:
                game.cruiserSprite.spriteSheet = game.cruiserSpriteSheet;
                break;
            case 3:
                game.subSprite.spriteSheet = game.submarineSpriteSheet;
                break;
            case 4:
                game.destroyerSprite.spriteSheet = game.destroyerSpriteSheet;
                break;
            }
        }

    }
    hub.client.test = function(myObj) {
        toastr.info(myObj);
    }

    hub.client.game_PlayerShotResult = function(offensivePlayer, deffensivePlayer, successful, shotLocation) {
        var split = shotLocation.split(',');
        var x = parseInt(split[0]);
        var y = parseInt(split[1]);

        if (playerId == offensivePlayer) {
            if (successful == false) {
                game.missSprite = new createjs.Sprite(game.missSpriteSheet);
                game.missSprite.x = (x * 32) + 480;
                game.missSprite.y = (y * 32) + 128;
                game.stage.addChild(game.missSprite);
            } else {
                //game.hitSprite = new createjs.Sprite(game.hitSpriteSheet);
                //game.hitSprite.x = (x * 32) + 480;
                //game.hitSprite.y = (y * 32) + 128;
                //game.stage.addChild(game.hitSprite);

                game.explosionAnimationSprite = new createjs.Sprite(game.explosionAnimationSpriteSheet, "run");
                game.explosionAnimationSprite.x = (x * 32) + 480;
                game.explosionAnimationSprite.y = (y * 32) + 128;
                game.stage.addChild(game.explosionAnimationSprite);
            }
        } else {
            if (successful == false) {
                game.missSprite = new createjs.Sprite(game.missSpriteSheet);
                game.missSprite.x = (x * 32) + 128;
                game.missSprite.y = (y * 32) + 128;
                game.stage.addChild(game.missSprite);
            } else {
                //game.hitSprite = new createjs.Sprite(game.hitSpriteSheet);
                //game.hitSprite.x = (x * 32) + 128;
                //game.hitSprite.y = (y * 32) + 128;
                //game.stage.addChild(game.hitSprite);

                game.explosionAnimationSprite = new createjs.Sprite(game.explosionAnimationSpriteSheet, "run");
                game.explosionAnimationSprite.x = (x * 32) + 128;
                game.explosionAnimationSprite.y = (y * 32) + 128;
                game.stage.addChild(game.explosionAnimationSprite);
            }
        }
    }


    hub.client.game_ShipSunk = function(offensivePlayer, deffensivePlayer, shipType) {
        toastr.info('Player ' + deffensivePlayer + ' has had their ship of type ' + shipType + ' sunk by  ' + offensivePlayer);
    }


    hub.client.game_GameOver = function(winningPlayer, losingPlayer) {
        //toastr.info('Player ' + winningPlayer + ' has won');
        //toastr.info('Player ' + losingPlayer + ' has lost');

        game.screenManager.changeScreen("gameOver");
        if (winningPlayer == playerId) {
            if (game.txtWinningMessage != null) {
                game.txtWinningMessage.text = "You WON!!!!";
                game.txtWinningMessage.updateText();
            }
        } else {
            if (game.txtWinningMessage != null) {
                game.txtWinningMessage.text = "Better luck next time.";
                game.txtWinningMessage.updateText();
            }

        }
    }


    hub.client.game_NewPlayerTurn = function(newCurrentPlayer) {
        //toastr.info('New Current Player ' + newCurrentPlayer);
        if (newCurrentPlayer == playerId) {
            if (game.txtCurrentPlayerMessage != null) {
                game.txtCurrentPlayerMessage.text = "It is your turn.";
                game.txtCurrentPlayerMessage.updateText();
            }
        } else {
            if (game.txtCurrentPlayerMessage != null) {
                game.txtCurrentPlayerMessage.text = "Waiting for other player.";
                game.txtCurrentPlayerMessage.updateText();
            }

        }
    }


    $.connection.hub
        .start()
        .done(function() {
            hub.server.sendAction(JSON.stringify(
            {
                action: 'ClientConnection'
            }));

            canExecute = true;
        });
});