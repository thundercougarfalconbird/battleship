﻿var ui = function() {

    this.blueSheetPath = "./content/ui/blueSheet.png";
    this.greenSheetPath = "./content/ui/greenSheet.png";
    this.greySheetPath = "./content/ui/greySheet.png";
    this.redSheetPath = "./content/ui/redSheet.png";
    this.yellowSheetPath = "./content/ui/yellowSheet.png";

    this.theme = "red";

    this.spriteSheet = null;

    this.controls = [];

    this.fontName = "20px Arial";

    /////////////////////////////////////////
    // Changes the UI Theme
    /////////////////////////////////////////
    this.changeTheme = function(themeName) {
        this.theme = themeName;
        this.loadContent();
    }

    /////////////////////////////////////////
    // Gets the path of the sprite sheet based on theme selected
    /////////////////////////////////////////
    this.getThemePath = function() {
        if (this.theme == "blue") return this.blueSheetPath;
        if (this.theme == "green") return this.greenSheetPath;
        if (this.theme == "grey") return this.greySheetPath;
        if (this.theme == "red") return this.redSheetPath;
        if (this.theme == "yellow") return this.yellowSheetPath;
        return this.greySheetPath;
    }

    /////////////////////////////////////////
    // Adds a control the the UI listing
    /////////////////////////////////////////
    this.addControl = function(control) {
        var index = this.controls.length;
        this.controls[index] = control;
        game.stage.addChild(control.sprite);
    }

    /////////////////////////////////////////
    // Loads the UI's base content
    /////////////////////////////////////////
    this.loadContent = function() {
        this.spriteSheet = new createjs.SpriteSheet({
            images: [this.getThemePath()],
            frames: [
                [380, 36, 38, 36],
                [380, 0, 38, 36],
                [386, 210, 36, 36],
                [0, 94, 190, 49],
                [190, 49, 190, 45],
                [190, 0, 190, 49],
                [0, 49, 190, 45],
                [0, 0, 190, 49],
                [0, 192, 190, 45],
                [288, 194, 49, 49],
                [239, 194, 49, 49],
                [190, 194, 49, 45],
                [339, 94, 49, 49],
                [290, 94, 49, 49],
                [337, 184, 49, 49],
                [290, 139, 49, 49],
                [0, 143, 190, 49],
                [337, 233, 21, 20],
                [386, 174, 36, 36],
                [0, 237, 18, 18],
                [190, 94, 100, 100],
                [416, 72, 28, 43],
                [339, 143, 39, 31],
                [378, 143, 39, 31],
                [388, 72, 28, 42],
                [18, 239, 17, 17],
            ],
            animations: {
                boxCheckmark: 0,
                boxCross: 1,
                boxTick: 2,
                button00: 3,
                button01: 4,
                button02: 5,
                button03: 6,
                button04: 7,
                button05: 8,
                button06: 9,
                button07: 10,
                button08: 11,
                button09: 12,
                button10: 13,
                button11: 14,
                button12: 15,
                button13: 16,
                checkmark: 17,
                circle: 18,
                cross: 19,
                panel: 20,
                sliderDown: 21,
                sliderLeft: 22,
                sliderRight: 23,
                sliderUp: 24,
                tick: 25,
                buttonClick: {
                    frames: [6, 6, 6, 6, 5],
                    speed: 0.5,
                    next: "button02"
                },
            }
        });
    }
}

/////////////////////////////////////////
// A Generic Button
/////////////////////////////////////////
var button = function(x, y) {
    this.action = null;
    this.x = x;
    this.y = y;
    this.sprite = null;
    this.text = "button";
    this.textColor = "#ffffff";
    this.textSprite = null;
    this.textOffsetX = 8;
    this.textOffsetY = 11;
    this.isVisible = true;

    this.initialize = function() {
        this.sprite = new createjs.Sprite(game.ui.spriteSheet, "button02");
        this.sprite.x = this.x;
        this.sprite.y = this.y;
        this.sprite.framerate = 60;
        game.ui.addControl(this);

        this.textSprite = new createjs.Text(this.text, game.ui.fontName, this.textColor);
        this.textSprite.x = this.x + this.textOffsetX;
        this.textSprite.y = this.y + this.textOffsetY;
        game.stage.addChild(this.textSprite);

        this.sprite.on("mouseover", this.onMouseOver);
        this.sprite.on("mouseout", this.onMouseOut);
        this.sprite.on("click", this.onClick);
        if (this.action != null) this.sprite.on("click", this.action);
    }

    this.update = function() {
        this.sprite.visible = this.isVisible;
        this.textSprite.visible = this.isVisible;
    }

    this.onMouseOver = function(evt) {
        var sprite = evt.target;
        sprite.gotoAndPlay("button00");
    }

    this.onMouseOut = function(evt) {
        var sprite = evt.target;
        sprite.gotoAndPlay("button02");
    }

    this.onClick = function(evt) {
        var sprite = evt.target;
        createjs.Tween.get(sprite, { loop: false })
            .to({ y: sprite.y + 4 }, 50, createjs.Ease.getPowInOut(4))
            .to({ y: sprite.y }, 50, createjs.Ease.getPowInOut(4));
    }
}

/////////////////////////////////////////
// A Generic Label
/////////////////////////////////////////
var label = function(x, y, text) {
    this.x = x;
    this.y = y;
    this.text = text;
    this.textColor = "#ffffff";
    this.textSprite = null;

    this.initialize = function() {
        this.textSprite = new createjs.Text(this.text, game.ui.fontName, this.textColor);
        this.textSprite.x = this.x;
        this.textSprite.y = this.y;
        game.stage.addChild(this.textSprite);
    }

    this.update = function() {

    }

    this.updateText = function() {
        this.textSprite.text = this.text;
    }
}