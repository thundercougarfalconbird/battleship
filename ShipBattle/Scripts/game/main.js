﻿/////////////////////////////////
// Variables
/////////////////////////////////
var settings = new settings();
var game = null;


/////////////////////////////////
// Entry Point
/////////////////////////////////
function initializeGame() {
    WriteToConsole("InitializeGame()");

    game = new game();
    game.initialize();
    game.loadContent();
}

/////////////////////////////////
// Game
/////////////////////////////////
var game = function() {
    this.stage = null;
    this.renderer = null;
    this.ui = null;

    /////////////////////////////////////////
    // Controls
    /////////////////////////////////////////

    // Global assets
    this.waterCellSpriteSheet = null;
    this.waterCellSprite = null;
    this.gridSpriteSheet = null;
    this.gridSprites = [];

    this.carrierSpriteSheet = null;
    this.carrierRedSpriteSheet = null;

    this.battleshipSprite = null;
    this.battleshipRedSpriteSheet = null;

    this.cruiserSprite = null;
    this.cruiserRedSpriteSheet = null;

    this.subSprite = null;
    this.subRedSpriteSheet = null;

    this.destroyerSprite = null;
    this.destroyerRedSpriteSheet = null;

    this.carrierX = -1;
    this.carrierY = -1;
    this.carrierRotation = 0;
    this.battleshipX = -1;
    this.battleshipY = -1;
    this.battleshipRotation = 0;
    this.cruiserX = -1;
    this.cruiserY = -1;
    this.cruiserRotation = 0;
    this.subX = -1;
    this.subY = -1;
    this.subRotation = 0;
    this.destroyerX = -1;
    this.destroyerY = -1;
    this.destroyerRotation = 0;

    // Title screen assets
    this.btnCreateGame = null;
    this.btnJoinGame = null;
    this.titleSprite = null;
    this.txtPlayersWaiting = null;
    this.txtPlayersHosting = null;
    this.txtActiveGames = null;
    this.selectedPiece = -1;

    // Start screen assets
    this.carrierSprite = null;
    this.battleshipSprite = null;
    this.cruiserSprite = null;
    this.subSprite = null;
    this.destroyerSprite = null;
    this.btnRotate = null;
    this.btnReady = null;

    // Game screen assets
    // Reuse of carrier/battleShip/cruiser/sub/destroyer sprites
    this.txtTurnIndicator = null;
    this.missSpriteSheet = null;
    this.hitSpriteSheet = null;
    this.explosionAnimationSpriteSheet = null;
    this.explosionAnimationSprite = null;

    // Game over screen assets
    this.txtResults = null;
    this.txtShotsFired = null;
    this.txtRounds = null;
    this.btnStartNewGame = null;


    /////////////////////////////////////////
    // *NOTE* ensure you are using game.* for game variables
    // this.* will keep referencing the browser window for some reason
    // This is only applicable in the game class
    /////////////////////////////////////////

    /////////////////////////////////////////
    // Initializes the game
    /////////////////////////////////////////
    this.initialize = function() {
        game.ui = new ui();
        game.screenManager = new screenManager();

        game.stage = new createjs.Stage("gameCanvas");
        game.stage.enableMouseOver();

        createjs.Ticker.addEventListener("tick", game.update);
        createjs.Ticker.useRAF = true;
        createjs.Ticker.setFPS(60);

        game.screenManager.add("title", new titleScreen());
        game.screenManager.add("start", new startScreen());
        game.screenManager.add("game", new gameScreen());
        game.screenManager.add("gameOver", new gameOverScreen());

    }

    /////////////////////////////////////////
    // Main update loop of game
    /////////////////////////////////////////
    this.update = function() {
        try {
            game.ui.controls.forEach(function(control) {
                control.update();
            });
            game.screenManager.update();

            game.stage.update();
        } catch (ex) {
            WriteToConsole(ex);
        }
    }

    /////////////////////////////////////////
    // Load game content
    /////////////////////////////////////////
    this.loadContent = function() {
        game.ui.loadContent();

        game.currentPlayerText = new createjs.Text("Current Player: ", game.ui.fontName, "#ffffff");
        game.currentPlayerText.x = 0;
        game.currentPlayerText.y = 0;
        game.stage.addChild(game.currentPlayerText);

        game.screenManager.changeScreen("title");
    }

    this.getShipX = function(shipType) {
        var sprite = game.carrierSprite;
        switch (shipType) {
        case 0:
            sprite = game.carrierSprite;
            break;
        case 1:
            sprite = game.battleshipSprite;
            break;
        case 2:
            sprite = game.cruiserSprite;
            break;
        case 3:
            sprite = game.subSprite;
            break;
        case 4:
            sprite = game.destroyerSprite;
            break;
        }

        var x = (sprite.x - 320) / 32;
        var y = (sprite.y - 128) / 32;
        return x;
    }

    this.getShipY = function(shipType) {
        var sprite = game.carrierSprite;
        switch (shipType) {
        case 0:
            sprite = game.carrierSprite;
            break;
        case 1:
            sprite = game.battleshipSprite;
            break;
        case 2:
            sprite = game.cruiserSprite;
            break;
        case 3:
            sprite = game.subSprite;
            break;
        case 4:
            sprite = game.destroyerSprite;
            break;
        }

        var x = (sprite.x - 320) / 32;
        var y = (sprite.y - 128) / 32;
        return y;
    }

    this.getShipRotation = function(shipType) {
        var sprite = game.carrierSprite;
        switch (shipType) {
        case 0:
            sprite = game.carrierSprite;
            break;
        case 1:
            sprite = game.battleshipSprite;
            break;
        case 2:
            sprite = game.cruiserSprite;
            break;
        case 3:
            sprite = game.subSprite;
            break;
        case 4:
            sprite = game.destroyerSprite;
            break;
        }

        var r = sprite.rotation;
        return r;
    }
}