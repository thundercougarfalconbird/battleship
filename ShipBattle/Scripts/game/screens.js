﻿/////////////////////////////////////////
// Base Screen
/////////////////////////////////////////
var screen = new function() {
    this.name = "";

    this.update = function() {}
    this.loadContent = function() {}
};

/////////////////////////////////////////
// Screen manager
/////////////////////////////////////////
function screenManager() {
    this.screens = [];
    this.screenNames = [];
    this.currentScreen = null;

    this.add = function(name, screen) {
        screen.name = name;
        this.screens[this.screens.length] = screen;
        this.screenNames[this.screenNames.length] = name;
    }

    this.changeScreen = function(name) {
        var index = this.screenNames.indexOf(name);
        this.currentScreen = this.screens[index];
        game.stage.removeAllChildren();
        this.currentScreen.loadContent();
    }

    this.update = function() {
        this.currentScreen.update();
    }

    this.get = function(name) {
        var index = this.screenNames.indexOf(name);
        return this.screens[index];
    }
}

/////////////////////////////////////////
// Template Screen
/////////////////////////////////////////
function templateScreen() {
    this.name = "template";

    this.loadContent = function() {

    }

    this.update = function() {

    }
}

/////////////////////////////////////////
// Title Screen
/////////////////////////////////////////
function titleScreen() {
    this.name = "title";

    this.loadContent = function() {

        /////////////////////////////////////////
        // Assets
        /////////////////////////////////////////
        game.waterCellSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/water-a.png"],
            frames: [
                [0, 0, 32, 32],
                [32, 0, 32, 32],
                [64, 0, 32, 32]
            ],
            animations: {
                run: {
                    frames: [0, 1, 2, 1],
                    speed: 0.05
                }
            }
        });
        game.waterCellSprite = new createjs.Sprite(game.waterCellSpriteSheet);

        for (var y = 0; y < 720; y += 32) {
            for (var x = 0; x < 1280; x += 32) {
                var sprite = new createjs.Sprite(game.waterCellSpriteSheet, "run");
                sprite.x = x;
                sprite.y = y;
                game.stage.addChild(sprite);
            }
        }

        /////////////////////////////////////////
        // Controls
        /////////////////////////////////////////        
        //game.btnCreateGame = new button(128, 512);
        //game.btnCreateGame.text = "Create Game";
        //game.btnCreateGame.action = function (evt) {
        //    CreateGame();
        //};
        //game.btnCreateGame.initialize();

        game.btnJoinGame = new button(128, 592);
        game.btnJoinGame.text = "Play Game";
        game.btnJoinGame.action = function(evt) {
            JoinGame();
        };
        game.btnJoinGame.initialize();

        game.btnJoinSinglePlayerGame = new button(128, 650);
        game.btnJoinSinglePlayerGame.text = "Play Single Player Game";
        game.btnJoinSinglePlayerGame.action = function(evt) {
            JoinSinglePlayerGame();
        };
        game.btnJoinSinglePlayerGame.initialize();


        var titleSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/title.png"],
            frames: [[0, 0, 770, 150]]
        });
        game.titleSprite = new createjs.Sprite(titleSpriteSheet);
        game.titleSprite.x = (1280 / 2) - (770 / 2);
        game.titleSprite.y = 128;
        game.stage.addChild(game.titleSprite);

        game.txtPlayersWaiting = new label(960, 512, "Total Games: 0");
        game.txtPlayersWaiting.initialize();

        game.txtPlayersHosting = new label(960, 544, "Games Waiting for Players: 0");
        game.txtPlayersHosting.initialize();
    }

    this.update = function() {

    }
}

/////////////////////////////////////////
// Start Screen
/////////////////////////////////////////
function startScreen() {
    this.name = "start";

    this.loadContent = function() {
        /////////////////////////////////////////
        // Events
        /////////////////////////////////////////
        //game.stage.removeEventListener("mouseover");
        //game.stage.addEventListener("mouseover", this.mouseMove);

        /////////////////////////////////////////
        // Assets
        /////////////////////////////////////////
        game.waterCellSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/water-a.png"],
            frames: [
                [0, 0, 32, 32],
                [32, 0, 32, 32],
                [64, 0, 32, 32]
            ],
            animations: {
                run: {
                    frames: [0, 1, 2, 1],
                    speed: 0.05
                }
            }
        });
        game.waterCellSprite = new createjs.Sprite(game.waterCellSpriteSheet);

        game.gridSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/grid2.png"],
            frames: [[0, 0, 32, 32]]
        });

        game.gridHighlightSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/grid-h.png"],
            frames: [[0, 0, 32, 32]]
        });

        for (var y = 0; y < 720; y += 32) {
            for (var x = 0; x < 1280; x += 32) {
                var sprite = new createjs.Sprite(game.waterCellSpriteSheet, "run");
                sprite.x = x;
                sprite.y = y;
                game.stage.addChild(sprite);
            }
        }

        for (var y = 0; y < 320; y += 32) {
            for (var x = 0; x < 320; x += 32) {
                var sprite = new createjs.Sprite(game.gridSpriteSheet);
                sprite.x = x + 320;
                sprite.y = y + 128;
                sprite.addEventListener("mouseover", this.mouseMove);
                sprite.addEventListener("click", this.mouseClick);
                game.gridSprites[game.gridSprites.length] = sprite;
                game.stage.addChild(sprite);
            }
        }

        /////////////////////////////////////////
        // Ships
        /////////////////////////////////////////        

        game.carrierSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/carrier.png"],
            frames: [[0, 0, 565, 213]]
        });
        game.carrierRedSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/carrier-r.png"],
            frames: [[0, 0, 565, 213]]
        });
        game.carrierSprite = new createjs.Sprite(game.carrierSpriteSheet);
        game.carrierSprite.x = 1024;
        game.carrierSprite.y = 128;
        game.carrierSprite.scaleX = 0.275;
        game.carrierSprite.scaleY = 0.15;
        game.carrierSprite.rotation = 0;
        game.carrierSprite.addEventListener("click", this.carrierClick);
        game.stage.addChild(game.carrierSprite);

        game.battleshipSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/battleship.png"],
            frames: [[0, 0, 555, 123]]
        });
        game.battleshipRedSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/battleship-r.png"],
            frames: [[0, 0, 555, 123]]
        });
        game.battleshipSprite = new createjs.Sprite(game.battleshipSpriteSheet);
        game.battleshipSprite.x = 1024;
        game.battleshipSprite.y = 192;
        game.battleshipSprite.scaleX = 0.225;
        game.battleshipSprite.scaleY = 0.25;
        game.battleshipSprite.rotation = 0;
        game.battleshipSprite.addEventListener("click", this.battleShipClick);
        game.stage.addChild(game.battleshipSprite);

        game.cruiserSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/cruiser.png"],
            frames: [[0, 0, 400, 69]]
        });
        game.cruiserRedSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/cruiser-r.png"],
            frames: [[0, 0, 400, 69]]
        });
        game.cruiserSprite = new createjs.Sprite(game.cruiserSpriteSheet);
        game.cruiserSprite.x = 1024;
        game.cruiserSprite.y = 256;
        game.cruiserSprite.scaleX = 0.25;
        game.cruiserSprite.scaleY = 0.45;
        game.cruiserSprite.rotation = 0;
        game.cruiserSprite.addEventListener("click", this.cruiserClick);
        game.stage.addChild(game.cruiserSprite);

        game.submarineSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/submarine.png"],
            frames: [[0, 0, 420, 90]]
        });
        game.submarineRedSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/submarine-r.png"],
            frames: [[0, 0, 420, 90]]
        });
        game.subSprite = new createjs.Sprite(game.submarineSpriteSheet);
        game.subSprite.x = 1024;
        game.subSprite.y = 320;
        game.subSprite.scaleX = 0.235;
        game.subSprite.scaleY = 0.36;
        game.subSprite.rotation = 0;
        game.subSprite.addEventListener("click", this.submarineClick);
        game.stage.addChild(game.subSprite);

        game.destroyerSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/destroyer.png"],
            frames: [[0, 0, 335, 59]]
        });
        game.destroyerRedSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/destroyer-r.png"],
            frames: [[0, 0, 335, 59]]
        });
        game.destroyerSprite = new createjs.Sprite(game.destroyerSpriteSheet);
        game.destroyerSprite.x = 1024;
        game.destroyerSprite.y = 384;
        game.destroyerSprite.scaleX = 0.19;
        game.destroyerSprite.scaleY = 0.52;
        game.destroyerSprite.rotation = 0;
        game.destroyerSprite.addEventListener("click", this.destroyerClick);
        game.stage.addChild(game.destroyerSprite);

        game.carrierRotation = 0;
        game.battleshipRotation = 0;
        game.cruiserRotation = 0;
        game.subRotation = 0;
        game.destroyerRotation = 0;

        /////////////////////////////////////////
        // Controls
        /////////////////////////////////////////     

        game.btnReady = new button(1024, 576);
        game.btnReady.text = "Ready";
        game.btnReady.isVisible = false;
        game.btnReady.action = function(evt) {
            SetReady();
        };
        game.btnReady.initialize();

        game.btnRotate = new button(1024, 448);
        game.btnRotate.text = "Rotate";
        game.btnRotate.action = function(evt) {
            Rotate();
        };
        game.btnRotate.initialize();
    }

    this.update = function() {
        var i = 0;
    }

    this.mouseMove = function(evt) {
        for (var i = 0; i < game.gridSprites.length; i++) {
            var gridSprite = game.gridSprites[i];
            gridSprite.spriteSheet = game.gridSpriteSheet;
        }

        var sprite = evt.currentTarget;
        sprite.spriteSheet = game.gridHighlightSpriteSheet;
    }

    this.mouseClick = function(evt) {
        var sprite = evt.currentTarget;
        var index = game.gridSprites.indexOf(sprite);
        var x = index % 10;
        var y = (index - x) / 10;
        //toastr.info("X: " + x + ", Y: " + y);

        if (game.selectedPiece == -1) return;

        hub.server.sendAction(JSON.stringify(
        {
            action: 'PlaceShip',
            gameId: gameId,
            ship: game.selectedPiece + 1,
            startPoint: x + ", " + y,
            isUpward: false
        }));

        switch (game.selectedPiece) {
        case 0: // carrier
            game.carrierSprite.rotation = 0;
            game.carrierSprite.x = (x * 32) + 320;
            game.carrierSprite.y = (y * 32) + 128;
            break;
        case 1: // battleship
            game.battleshipSprite.rotation = 0;
            game.battleshipSprite.x = (x * 32) + 320;
            game.battleshipSprite.y = (y * 32) + 128;
            break;
        case 2: // cruiser
            game.cruiserSprite.rotation = 0;
            game.cruiserSprite.x = (x * 32) + 320;
            game.cruiserSprite.y = (y * 32) + 128;
            break;
        case 3: // submarine
            game.subSprite.rotation = 0;
            game.subSprite.x = (x * 32) + 320;
            game.subSprite.y = (y * 32) + 128;
            break;
        case 4: // destroyer
            game.destroyerSprite.rotation = 0;
            game.destroyerSprite.x = (x * 32) + 320;
            game.destroyerSprite.y = (y * 32) + 128;
            break;
        }
    }

    this.carrierClick = function(evt) {
        game.selectedPiece = 0;
        toastr.info("Carrier Selected");
    }

    this.battleShipClick = function(evt) {
        game.selectedPiece = 1;
        toastr.info("Battleship Selected");
    }

    this.cruiserClick = function(evt) {
        game.selectedPiece = 2;
        toastr.info("Cruiser Selected");
    }

    this.submarineClick = function(evt) {
        game.selectedPiece = 3;
        toastr.info("Submarine Selected");
    }

    this.destroyerClick = function(evt) {
        game.selectedPiece = 4;
        toastr.info("Destroyer Selected");
    }

    this.rotate = function(evt) {
        if (game.selectedPiece == -1) return;

        var isUpward = false;
        var sprite = null;
        var x = -1;
        var y = -1;

        switch (game.selectedPiece) {
        case 0: // carrier
            sprite = game.carrierSprite;
            x = (sprite.x - 320) / 32;
            y = (sprite.y - 128) / 32;
            if (game.carrierSprite.rotation == 0) {
                game.carrierSprite.rotation = -90;
                game.carrierSprite.y += 32;
                isUpward = true;
            } else {
                game.carrierSprite.rotation = 0;
                game.carrierSprite.y -= 32;
            }
            break;
        case 1: // battleship
            sprite = game.battleshipSprite;
            x = (sprite.x - 320) / 32;
            y = (sprite.y - 128) / 32;
            if (game.battleshipSprite.rotation == 0) {
                game.battleshipSprite.rotation = -90;
                game.battleshipSprite.y += 32;
                isUpward = true;
            } else {
                game.battleshipSprite.rotation = 0;
                game.battleshipSprite.y -= 32;
            }
            break;
        case 2: // cruiser
            sprite = game.cruiserSprite;
            x = (sprite.x - 320) / 32;
            y = (sprite.y - 128) / 32;
            if (game.cruiserSprite.rotation == 0) {
                game.cruiserSprite.rotation = -90;
                game.cruiserSprite.y += 32;
                isUpward = true;
            } else {
                game.cruiserSprite.rotation = 0;
                game.cruiserSprite.y -= 32;
            }
            break;
        case 3: // submarine
            sprite = game.subSprite;
            x = (sprite.x - 320) / 32;
            y = (sprite.y - 128) / 32;
            if (game.subSprite.rotation == 0) {
                game.subSprite.rotation = -90;
                game.subSprite.y += 32;
                isUpward = true;
            } else {
                game.subSprite.rotation = 0;
                game.subSprite.y -= 32;
            }
            break;
        case 4: // destroyer
            sprite = game.destroyerSprite;
            x = (sprite.x - 320) / 32;
            y = (sprite.y - 128) / 32;
            if (game.destroyerSprite.rotation == 0) {
                game.destroyerSprite.rotation = -90;
                game.destroyerSprite.y += 32;
                isUpward = true;
            } else {
                game.destroyerSprite.rotation = 0;
                game.destroyerSprite.y -= 32;
            }
            break;
        }

        hub.server.sendAction(JSON.stringify(
        {
            action: 'PlaceShip',
            gameId: gameId,
            ship: game.selectedPiece + 1,
            startPoint: x + ", " + y,
            isUpward: isUpward
        }));
    }
}

/////////////////////////////////////////
// Game Screen
/////////////////////////////////////////
function gameScreen() {
    this.name = "game";

    this.loadContent = function() {
        /////////////////////////////////////////
        // Assets
        /////////////////////////////////////////
        game.waterCellSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/water-a.png"],
            frames: [
                [0, 0, 32, 32],
                [32, 0, 32, 32],
                [64, 0, 32, 32]
            ],
            animations: {
                run: {
                    frames: [0, 1, 2, 1],
                    speed: 0.05
                }
            }
        });
        game.waterCellSprite = new createjs.Sprite(game.waterCellSpriteSheet);

        game.gridSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/grid2.png"],
            frames: [[0, 0, 32, 32]]
        });

        game.gridHighlightSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/grid-h.png"],
            frames: [[0, 0, 32, 32]]
        });

        game.explosionAnimationSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/explosion-a.png"],
            frames: [
                [0, 0, 32, 32],
                [32, 0, 32, 32],
                [64, 0, 32, 32],
                [96, 0, 32, 32],
                [128, 0, 32, 32],
                [160, 0, 32, 32],
                [192, 0, 32, 32],
                [224, 0, 32, 32],
                [256, 0, 32, 32],
                [288, 0, 32, 32]
            ],
            animations: {
                run: {
                    frames: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                    speed: 1,
                    next: "crater"
                },
                crater: {
                    frames: [9],
                    speed: 1
                }
            }
        });

        for (var y = 0; y < 720; y += 32) {
            for (var x = 0; x < 1280; x += 32) {
                var sprite = new createjs.Sprite(game.waterCellSpriteSheet, "run");
                sprite.x = x;
                sprite.y = y;
                game.stage.addChild(sprite);
            }
        }

        for (var y = 0; y < 320; y += 32) {
            for (var x = 0; x < 320; x += 32) {
                var sprite = new createjs.Sprite(game.gridSpriteSheet);
                sprite.x = x + 128;
                sprite.y = y + 128;
                game.stage.addChild(sprite);
            }
        }

        game.gridSprites = [];

        for (var y = 0; y < 320; y += 32) {
            for (var x = 0; x < 320; x += 32) {
                var sprite = new createjs.Sprite(game.gridSpriteSheet);
                sprite.x = x + 480;
                sprite.y = y + 128;
                sprite.addEventListener("mouseover", this.mouseMove);
                sprite.addEventListener("click", this.mouseClick);
                game.gridSprites[game.gridSprites.length] = sprite;
                game.stage.addChild(sprite);
            }
        }

        /////////////////////////////////////////
        // Ships
        /////////////////////////////////////////        

        var carrierSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/carrier.png"],
            frames: [[0, 0, 565, 213]]
        });
        game.carrierSprite = new createjs.Sprite(carrierSpriteSheet);
        game.carrierSprite.x = (game.carrierX * 32) + 128;
        game.carrierSprite.y = (game.carrierY * 32) + 128;
        game.carrierSprite.scaleX = 0.275;
        game.carrierSprite.scaleY = 0.15;
        game.carrierSprite.rotation = game.carrierRotation;
        game.stage.addChild(game.carrierSprite);

        var battleshipSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/battleship.png"],
            frames: [[0, 0, 555, 123]]
        });
        game.battleshipSprite = new createjs.Sprite(battleshipSpriteSheet);
        game.battleshipSprite.x = (game.battleshipX * 32) + 128;
        game.battleshipSprite.y = (game.battleshipY * 32) + 128;
        game.battleshipSprite.scaleX = 0.225;
        game.battleshipSprite.scaleY = 0.25;
        game.battleshipSprite.rotation = game.battleshipRotation;
        game.stage.addChild(game.battleshipSprite);

        var cruiserSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/cruiser.png"],
            frames: [[0, 0, 400, 69]]
        });
        game.cruiserSprite = new createjs.Sprite(cruiserSpriteSheet);
        game.cruiserSprite.x = (game.cruiserX * 32) + 128;
        game.cruiserSprite.y = (game.cruiserY * 32) + 128;
        game.cruiserSprite.scaleX = 0.25;
        game.cruiserSprite.scaleY = 0.45;
        game.cruiserSprite.rotation = game.cruiserRotation;
        game.stage.addChild(game.cruiserSprite);

        var submarineSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/submarine.png"],
            frames: [[0, 0, 420, 90]]
        });
        game.subSprite = new createjs.Sprite(submarineSpriteSheet);
        game.subSprite.x = (game.subX * 32) + 128;
        game.subSprite.y = (game.subY * 32) + 128;
        game.subSprite.scaleX = 0.235;
        game.subSprite.scaleY = 0.36;
        game.subSprite.rotation = game.subRotation;
        game.stage.addChild(game.subSprite);

        var destroyerSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/destroyer.png"],
            frames: [[0, 0, 335, 59]]
        });
        game.destroyerSprite = new createjs.Sprite(destroyerSpriteSheet);
        game.destroyerSprite.x = (game.destroyerX * 32) + 128;
        game.destroyerSprite.y = (game.destroyerY * 32) + 128;
        game.destroyerSprite.scaleX = 0.19;
        game.destroyerSprite.scaleY = 0.52;
        game.destroyerSprite.rotation = game.destroyerRotation;
        game.stage.addChild(game.destroyerSprite);

        game.missSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/splash.png"],
            frames: [[0, 0, 32, 32]]
        });

        game.hitSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/explosion.png"],
            frames: [[0, 0, 32, 32]]
        });


        game.txtCurrentPlayerMessage = new label(300, 64, "");
        game.txtCurrentPlayerMessage.initialize();

        /////////////////////////////////////////
        // Controls
        ///////////////////////////////////////// 
    }

    this.update = function() {

    }

    this.mouseMove = function(evt) {
        for (var i = 0; i < game.gridSprites.length; i++) {
            var gridSprite = game.gridSprites[i];
            gridSprite.spriteSheet = game.gridSpriteSheet;
        }

        var sprite = evt.currentTarget;
        sprite.spriteSheet = game.gridHighlightSpriteSheet;
    }

    this.mouseClick = function(evt) {
        var sprite = evt.currentTarget;
        var x = (sprite.x - 480) / 32;
        var y = (sprite.y - 128) / 32;
        FireShot(x, y);
    }

    function FireShot(x, y) {
        if (canExecute)
            hub.server.sendAction(JSON.stringify(
            {
                action: 'FireShot',
                gameId: gameId,
                position: x + ', ' + y
            }));
        else
            toastr.info("Please wait while servers load.");
    }

}

/////////////////////////////////////////
// Game Over Screen
/////////////////////////////////////////
function gameOverScreen() {
    this.name = "gameOver";

    this.loadContent = function() {
        /////////////////////////////////////////
        // Assets
        /////////////////////////////////////////
        game.waterCellSpriteSheet = new createjs.SpriteSheet({
            images: ["./content/water-a.png"],
            frames: [
                [0, 0, 32, 32],
                [32, 0, 32, 32],
                [64, 0, 32, 32]
            ],
            animations: {
                run: {
                    frames: [0, 1, 2, 1],
                    speed: 0.05
                }
            }
        });
        game.waterCellSprite = new createjs.Sprite(game.waterCellSpriteSheet);

        for (var y = 0; y < 720; y += 32) {
            for (var x = 0; x < 1280; x += 32) {
                var sprite = new createjs.Sprite(game.waterCellSpriteSheet, "run");
                sprite.x = x;
                sprite.y = y;
                game.stage.addChild(sprite);
            }
        }

        /////////////////////////////////////////
        // Controls
        ///////////////////////////////////////// 

        game.txtWinningMessage = new label(960, 512, "");
        game.txtWinningMessage.initialize();

        game.btnJoinGame = new button(128, 592);
        game.btnJoinGame.text = "Play another Game";
        game.btnJoinGame.action = function(evt) {
            JoinGame();
        };
        game.btnJoinGame.initialize();
    }

    this.update = function() {

    }
}