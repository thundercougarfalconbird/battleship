﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof (ShipBattle.Startup))]

namespace ShipBattle
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}